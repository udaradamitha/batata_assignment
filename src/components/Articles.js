import React, { useEffect, useState } from 'react';

const Articles = () => {

  const [totalPages, setTotalPages] = useState(0);
  const [articles, setArticles] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);
  const [inputvalue, setInputvalue] = useState('');

  const [store, setStore] = useState([]);


  const link = 'https://jsonmock.hackerrank.com/api/articles?page=';

  const apiCall = async (page) => {
    let filteredArticles = [];
    const pageData = [...store].findIndex(ele => ele.page === page - 1);
    if (pageData >= 0) {
      filteredArticles = [...store[pageData].data];
    }

    let url = link + page;
    let response = await fetch(url);
    let data = await response.json();

    let pages = data.total_pages;

    setTotalPages(pages);
    filteredArticles = data.data.filter((item) => item.title).concat(filteredArticles);
    setArticles(filteredArticles);
  }

  useEffect(() => {
    apiCall(1);
  }, []);

  const handleClick = async (index) => {
    setPageNumber(index);
    await apiCall(index + 1);
  }

  const insertNewWord = () => {
    articles.push({ title: inputvalue });
    setInputvalue('');
    const tmpStore = [...store];
    const pageData = tmpStore.findIndex(ele => ele.page === pageNumber);
    if (pageData >= 0) {
      const titleArray = tmpStore[pageData].data;
      titleArray.push({ title: inputvalue })
      tmpStore[pageData].data = titleArray;
    } else {
      tmpStore.push({
        page: pageNumber,
        data: [{ title: inputvalue }]
      })
    }
    setStore(tmpStore);
  }

  const handleInputChange = (e) => {
    setInputvalue(e.target.value);
  }

  const handleKeyPress = (event) => {
    if (event.key === 'Enter' && inputvalue !== '') {
      insertNewWord();
    }
  }

  return (

    <React.Fragment>
      <div className="pagination">
        {Array(totalPages).fill().map((page, index) => {
          return (
            <button data-testid="page-button" key={"page-button-" + index}
              className={`${pageNumber === index ? 'btn-selected' : null}`}
              onClick={() => { handleClick(index) }}>{index + 1}</button>
          )
        })
        }
      </div>

      <ul className="results">
        {articles.map((article, index) => {
          return (
            <li key={"title-" + index} data-testid="result-row">{article.title}</li>
          )
        })}
      </ul>
      <div>
        <input value={inputvalue} onChange={handleInputChange} onKeyPress={handleKeyPress} />
        <button onClick={insertNewWord} disabled={inputvalue === ''}>Add</button>
      </div>
    </React.Fragment>
  );
}

export default Articles;
